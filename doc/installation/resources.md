# Preparing Resources

To operate effectively, you will need to create a few resources before configuration and deployment of this chart.

You'll need a domain on which to host the services, which we will not cover here. You will need a [static IP](#static-ip) for the Ingress, so that you can create a [DNS entry](#dns-entry) for your hostnames.

## Static IP

(INSERT CREATING GCP STATIC IP HERE)

## DNS Entry

(INSERT ADDING GCP DNS ENTRY HERE)

Once all resources have been generated and recorded, you can proceed to generating
a [secrets](README.md#secrets).
